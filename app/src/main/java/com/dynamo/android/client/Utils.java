package com.dynamo.android.client;

import android.content.Context;
import android.content.res.TypedArray;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Utils {

    private static final String format = "MMMM dd";

    public static int getToolbarHeight(Context context) {
        final TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                new int[]{R.attr.actionBarSize});
        int toolbarHeight = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        return toolbarHeight;
    }

    public static String GetDisplayDate(String seconds) {
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTimeInMillis(Long.parseLong(seconds) * 1000);
        } catch (NumberFormatException e) {
            return null;
        }

        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);

        boolean sameDay = calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) &&
                calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR);

        if(sameDay) {
            return "today";
        }

        else if(calendar.before(today)) {
            final SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
            return sdf.format(calendar.getTime());
        }
        return null;
    }
}