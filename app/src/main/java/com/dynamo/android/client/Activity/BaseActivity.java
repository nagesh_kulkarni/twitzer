package com.dynamo.android.client.Activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ProgressBar;

import com.dynamo.android.client.Constant;
import com.dynamo.android.client.R;

public abstract class BaseActivity extends ActionBarActivity {

    private static final String TAG = BaseActivity.class.getSimpleName();

    private Toolbar mToolBar;
    private ProgressBar mProgressBar;
    private boolean isToolBarVisible;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Constant.DEBUG) Log.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);

        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        getToolbar();
    }

    public Toolbar getToolbar() {
        if (mToolBar == null) {
            mToolBar = (Toolbar) findViewById(R.id.toolbar);
            if (mToolBar != null) {
                setSupportActionBar(mToolBar);
                isToolBarVisible = true;
            }
        }
        return mToolBar;
    }

    public ProgressBar getProgressBar() {
        if (mProgressBar == null) {
            mProgressBar = (ProgressBar) findViewById(R.id.progressbar);
        }
        return mProgressBar;
    }

    public boolean isToolBarVisible() {
        return isToolBarVisible;
    }

    public void setToolBarVisibility(boolean isToolBarVisible) {
        this.isToolBarVisible = isToolBarVisible;
    }
}