package com.dynamo.android.client;

import android.app.Application;
import android.content.Context;

public class TwitzerApplication extends Application {

    private static Context mCtx;

    public void onCreate() {
        super.onCreate();

        mCtx = this.getApplicationContext();
    }

    public static Context getContext() {
        return mCtx;
    }
}
