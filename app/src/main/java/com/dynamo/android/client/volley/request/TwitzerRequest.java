package com.dynamo.android.client.volley.request;

import com.android.volley.Request;

public class TwitzerRequest extends BaseRequest {

    private static final String TWITZER_METHOD_URL = "twitzer";

    @Override
    public int getRequestMethodType() {
        return Request.Method.GET;
    }

    @Override
    protected String getMethodUrl() {
        return TWITZER_METHOD_URL;
    }
}
