package com.dynamo.android.client.Activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.dynamo.android.client.Fragment.TwitzFragment;
import com.dynamo.android.client.R;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment fragment = new TwitzFragment();
            fragmentManager.beginTransaction().replace(R.id.content_fragment, fragment).commit();
        }
    }
}
