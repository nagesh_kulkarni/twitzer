package com.dynamo.android.client;

public class Constant {

    /** If in Debug/development mode */
    public static final boolean DEBUG = BuildConfig.DEBUG;

    // Durations for certain animations we use:
    public static final int HEADER_HIDE_ANIM_DURATION = 300;
}
