package com.dynamo.android.client.Fragment;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.dynamo.android.client.Activity.BaseActivity;
import com.dynamo.android.client.Constant;
import com.dynamo.android.client.R;
import com.dynamo.android.client.adapter.TwitzAdapter;
import com.dynamo.android.client.common.view.CustomItemAnimator;
import com.dynamo.android.client.common.view.HidingScrollListener;
import com.dynamo.android.client.model.Twitz;
import com.dynamo.android.client.volley.VolleyClient;
import com.dynamo.android.client.volley.request.BaseRequest;
import com.dynamo.android.client.volley.request.TwitzerRequest;
import com.dynamo.android.client.volley.toolbox.GsonRequest;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import static com.dynamo.android.client.Utils.getToolbarHeight;

public class TwitzFragment extends BaseFragment {

    private static final String TAG = "TwitzFragment";

    private View mRootView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private List<Twitz> mTwitzs;
    private TwitzAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (Constant.DEBUG) Log.d(TAG, "onCreateView()");
        if (mRootView == null) {
            // Inflate the layout for this fragment
            mRootView = inflater.inflate(R.layout.fragment_twitz, container, false);

            mTwitzs = new ArrayList<>();

            // specify an adapter
            mAdapter = new TwitzAdapter(mTwitzs);

            addVolleyRequest();
        }
        return mRootView;
    }

    @Override
    public void onViewCreated(View view, final Bundle savedInstanceState) {
        if (Constant.DEBUG) Log.d(TAG, "onViewCreated()");
        super.onViewCreated(view, savedInstanceState);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        final RecyclerView mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        mRecyclerView.setItemAnimator(new CustomItemAnimator());

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.setOnScrollListener(new HidingScrollListener(((BaseActivity) getActivity()).isToolBarVisible()) {

            final View view = ((BaseActivity) getActivity()).getToolbar();

            @Override
            public void onHide() {
                ((BaseActivity) getActivity()).setToolBarVisibility(false);
                view.animate()
                        .translationY(-view.getBottom())
                        .alpha(0)
                        .setDuration(Constant.HEADER_HIDE_ANIM_DURATION)
                        .setInterpolator(new DecelerateInterpolator());
            }

            @Override
            public void onShow() {
                ((BaseActivity) getActivity()).setToolBarVisibility(true);
                view.animate()
                        .translationY(0)
                        .alpha(1)
                        .setDuration(Constant.HEADER_HIDE_ANIM_DURATION)
                        .setInterpolator(new DecelerateInterpolator());
            }
        });

        mAdapter.setOnItemClickListener(new TwitzAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (mSwipeRefreshLayout.isRefreshing()) {
                    return;
                }

                Twitz twitz = mTwitzs.get(position);
                // selected item
                Log.i(TAG, twitz.getId() + " clicked. Replacing fragment.");

                // We start the fragment transaction here. It is just an ordinary fragment transaction.
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_fragment,
                                DetailTwitzFragment.newInstance(twitz,
                                        (int) view.getX(), (int) view.getY(),
                                        view.getWidth(), view.getHeight())
                        )
                                // We push the fragment transaction to back stack. User can go back to the
                                // previous fragment by pressing back button.
                        .addToBackStack("detail")
                        .commit();
            }
        });

        mSwipeRefreshLayout.setProgressViewOffset(false, 0, getToolbarHeight(view.getContext()) + 30);
        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.refresh_progress_1,
                R.color.refresh_progress_2,
                R.color.refresh_progress_3);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                addVolleyRequest();
            }
        });
    }

    public void addVolleyRequest() {
        final BaseRequest twitzerRequest = new TwitzerRequest();
        final GsonRequest<List<Twitz>> gsonObjRequest = new GsonRequest<>(
                twitzerRequest.getRequestMethodType(),
                twitzerRequest.getApiUrl(),
                new TypeToken<ArrayList<Twitz>>() {
                }.getType(), null, new Response.Listener<List<Twitz>>() {

            @Override
            public void onResponse(List<Twitz> response) {
                if (!mTwitzs.isEmpty()) {
                    mAdapter.clearTwitz();
                    mTwitzs.clear();
                }
                int index = 0;
                for (Twitz twitz : response) {
                    mTwitzs.add(twitz);
                    mAdapter.notifyItemInserted(++index);
                }
                ((BaseActivity) getActivity()).getProgressBar().setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, new TwitzErrorHandler() {

            @Override
            public void onErrorResponse(VolleyError error) {
                super.onErrorResponse(error);
                mSwipeRefreshLayout.setRefreshing(false);
                ((BaseActivity) getActivity()).getProgressBar().setVisibility(View.GONE);
            }
        });
        gsonObjRequest.setTag(TAG);
        VolleyClient.getInstance(getActivity()).addToRequestQueue(gsonObjRequest);
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (Constant.DEBUG) Log.d(TAG, "onCreateAnimation()");
        return AnimationUtils.loadAnimation(getActivity(),
                enter ? android.R.anim.fade_in : android.R.anim.fade_out);
    }

    @Override
    public void onStop() {
        if (Constant.DEBUG) Log.d(TAG, "onStop()");
        super.onStop();
        VolleyClient.getInstance(getActivity()).getRequestQueue().cancelAll(TAG);
    }
}
