package com.dynamo.android.client.volley.request;

import android.net.Uri;

public abstract class BaseRequest {

    private static final String SCHEME = "http";
    private static final String AUTHORITY = "176.58.126.236";

    public abstract int getRequestMethodType();

    protected abstract String getMethodUrl();

    protected Uri.Builder getApiUriBuilder() {
        final Uri.Builder builder = new Uri.Builder();
        builder.scheme(SCHEME).authority(AUTHORITY);
        builder.appendPath(getMethodUrl());
        return builder;
    }

    public String getApiUrl() {
        return getApiUriBuilder().toString();
    }
}
