package com.dynamo.android.client.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.transition.Scene;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.dynamo.android.client.Activity.BaseActivity;
import com.dynamo.android.client.Constant;
import com.dynamo.android.client.model.Twitz;
import com.dynamo.android.client.R;
import com.dynamo.android.client.volley.VolleyClient;

import static com.dynamo.android.client.Utils.GetDisplayDate;
import static com.dynamo.android.client.Utils.getToolbarHeight;

public class DetailTwitzFragment extends BaseFragment implements Animation.AnimationListener {

    private static final String TAG = "DetailTwitzFragment";

    private static final String ARG_TWITZ = "twitz";
    private static final String ARG_X = "x";
    private static final String ARG_Y = "y";
    private static final String ARG_WIDTH = "width";
    private static final String ARG_HEIGHT = "height";

    public static DetailTwitzFragment newInstance(Twitz twitz,
                                                   int x, int y, int width, int height) {
        DetailTwitzFragment fragment = new DetailTwitzFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_TWITZ, twitz);
        args.putInt(ARG_X, x);
        args.putInt(ARG_Y, y);
        args.putInt(ARG_WIDTH, width);
        args.putInt(ARG_HEIGHT, height);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (Constant.DEBUG) Log.d(TAG, "onCreateView()");
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        if (Constant.DEBUG) Log.d(TAG, "onViewCreated()");
        ((BaseActivity) getActivity()).getProgressBar().setVisibility(View.GONE);
        FrameLayout root = (FrameLayout) view;
        Context context = view.getContext();

        // This is how the fragment looks at first. Since the transition is one-way, we don't need to make
        // this a Scene.
        View item = LayoutInflater.from(context).inflate(R.layout.item_twitz, root, false);
        assert item != null;
        bind(item);
        // We adjust the position of the initial image with LayoutParams using the values supplied
        // as the fragment arguments.
        Bundle args = getArguments();
        FrameLayout.LayoutParams params = null;
        if (args != null) {
            params = new FrameLayout.LayoutParams(args.getInt(ARG_WIDTH), args.getInt(ARG_HEIGHT));
            params.topMargin = args.getInt(ARG_Y);
            params.leftMargin = args.getInt(ARG_X);
        }
        root.addView(item, params);
    }

    private void bind(View parent) {
        Bundle args = getArguments();
        if (args == null) {
            return;
        }
        Twitz twitz = args.getParcelable(ARG_TWITZ);

        final NetworkImageView image = (NetworkImageView) parent.findViewById(R.id.image);
        image.setImageUrl(twitz.getImg(), VolleyClient.getInstance(getActivity()).getImageLoader());

        final TextView user = (TextView) parent.findViewById(R.id.user);
        user.setText(twitz.getUser());

        final TextView date = (TextView) parent.findViewById(R.id.date);
        final String displayDate = GetDisplayDate(twitz.getDate());
        if(TextUtils.isEmpty(displayDate)) {
            date.setVisibility(View.GONE);
        } else {
            date.setText(displayDate);
        }

        final TextView text = (TextView) parent.findViewById(R.id.text);
        text.setText(twitz.getFormatedTwitzText());
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (Constant.DEBUG) Log.d(TAG, "onCreateAnimation()");
        Animation animation = AnimationUtils.loadAnimation(getActivity(),
                enter ? android.R.anim.fade_in : android.R.anim.fade_out);
        // We bind a listener for the fragment transaction. We only bind it when
        // this fragment is entering.
        if (animation != null && enter) {
            animation.setAnimationListener(this);
        }
        return animation;
    }

    @Override
    public void onAnimationStart(Animation animation) {
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        // This method is called at the end of the animation for the fragment transaction,
        // which is perfect time to start our Transition.
        if (Constant.DEBUG) Log.d(TAG, "Fragment animation ended. Starting a Transition.");
        final Scene scene = Scene.getSceneForLayout((ViewGroup) getView(),
                R.layout.fragment_detail_content, getActivity());
        TransitionManager.go(scene);
        final View rootView = scene.getSceneRoot();
        if(((BaseActivity)getActivity()).isToolBarVisible()) {
            final FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.WRAP_CONTENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, getToolbarHeight(getActivity()), 0, 0);
            rootView.findViewById(R.id.container).setLayoutParams(lp);
        }
        bind(rootView);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }

}

