package com.dynamo.android.client.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.dynamo.android.client.TwitzerApplication;
import com.dynamo.android.client.R;
import com.dynamo.android.client.model.Twitz;
import com.dynamo.android.client.volley.VolleyClient;

import java.util.List;

import static com.dynamo.android.client.Utils.GetDisplayDate;

public class TwitzAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private final List<Twitz> mTwitzs;
    private final Context mCtx;
    OnItemClickListener mItemClickListener;

    public TwitzAdapter(List<Twitz> twitzs) {
        this.mCtx = TwitzerApplication.getContext();
        this.mTwitzs = twitzs;
    }

    public void clearTwitz() {
        int size = this.mTwitzs.size();
        for (int i = 0; i < size; i++) {
            mTwitzs.remove(0);
        }
        this.notifyItemRangeRemoved(0, size);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_header, parent, false);
            return new RecyclerHeaderViewHolder(view);
        }
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_twitz, parent, false);
        return new TwitzViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (isHeader(position)) {
            return;
        }
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        TwitzViewHolder holder = (TwitzViewHolder) viewHolder;
        final Twitz twitz = mTwitzs.get(position - 1); // Subtract 1 for header
        holder.image.setImageUrl(twitz.getImg(), VolleyClient.getInstance(mCtx).getImageLoader());
        holder.image.setErrorImageResId(android.R.drawable.ic_dialog_alert);
        holder.user.setText(twitz.getUser());

        final String date = GetDisplayDate(twitz.getDate());
        if(TextUtils.isEmpty(date)) {
            holder.date.setVisibility(View.GONE);
        } else {
            holder.date.setText(date);
        }
        holder.text.setText(twitz.getFormatedTwitzText());
    }

    @Override
    public int getItemViewType(int position) {
        return isHeader(position) ? TYPE_HEADER : TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return (mTwitzs == null ? 0 : mTwitzs.size()) + 1; // header
    }

    public boolean isHeader(int position) {
        return position == 0;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class TwitzViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        NetworkImageView image;
        TextView user;
        TextView date;
        TextView text;

        public TwitzViewHolder(View v) {
            super(v);
            image = (NetworkImageView) v.findViewById(R.id.image);
            user = (TextView) v.findViewById(R.id.user);
            date = (TextView) v.findViewById(R.id.date);
            text = (TextView) v.findViewById(R.id.text);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition() - 1); // Subtract 1 for header
            }
        }
    }

    public class RecyclerHeaderViewHolder extends RecyclerView.ViewHolder {
        public RecyclerHeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    public static interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

}